/**
 * When the page is loaded the we will load the function getSpaces()
 */
window.addEventListener('load', () => {
    getSpaces();
})


/**
 * Request the server, and then, we can call it whanever we want.
 */
const getSpaces = () => {
    fetch('http://localhost:8899/spaces').then((res) => {
        res.json().then((spaces) => {
            updateViewSpaces(spaces);
        })
    })
}


/**
 * Update the view, we receive all the spaces stored in our database. DOM manipulation.
 * 
 * @param {Array} spaces 
 */
const updateViewSpaces = (spaces) => {
    let template = document.querySelector("#template-spaces");
    
    spaces.forEach((v, i) => {
        let clone = document.importNode(template.content, true);
        
        clone.querySelector("h4").textContent = v.nomination;
        clone.querySelector("p").textContent = v.description;
        clone.querySelector("a").setAttribute("href", `http://localhost/space-detail.html?space=${v.id}`)
        clone.querySelector("#space-price").textContent = v.price + "€";
        
        document.querySelector("#space-content").appendChild(clone);
    })
}