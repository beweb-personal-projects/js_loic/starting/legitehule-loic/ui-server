window.addEventListener("load", () => {
    let id = new URLSearchParams(document.location.search).get("space");
    getSpace(id);
})

function getSpace(spacesID) {
    fetch(`http://localhost:8899/spaces/${spacesID}`).then((res) => {
        res.json().then((space) => {
            space === "error" ? window.location = "http://localhost" : updateViewSpace(space);
        })
    })
}

function updateViewSpace(space) {
    document.querySelector("#space-name").innerHTML = space.nomination;
    document.querySelector("#space-desc").innerHTML = space.description;
    document.querySelector("#space-price").innerHTML = space.price + "€";
}

