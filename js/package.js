window.addEventListener("load", () => {
    let id = new URLSearchParams(document.location.search).get("package");
    getPackage(id);
})

function getPackage(packageID) {
    fetch(`http://localhost:8899/packages/${packageID}`).then((res) => {
        res.json().then((package) => {
            package === "error" ? window.location = "http://localhost" : updateViewPackage(package);
        })
    })
}

function updateViewPackage(package) {
    document.querySelector("#package-name").innerHTML = package.nomination;
    document.querySelector("#package-desc").innerHTML = package.description;
    document.querySelector("#package-price").innerHTML = package.discount + "%";
}

