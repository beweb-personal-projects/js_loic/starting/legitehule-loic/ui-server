const SPACES = [
    {id: 1, nomination: "Room 1", description: "Room 1 Details", price: 35},
    {id: 2, nomination: "Room 2", description: "Room 2 Details", price: 35},
    {id: 3, nomination: "Appartement 1", description: "Appartement 1 Details", price: 110}
]

const PACKAGES = [
    {id: 1, nomination: "Pack 1", description: "Pack 1 Details", discount: 10},
    {id: 2, nomination: "Pack 2", description: "Pack 2 Details", discount: 5},
    {id: 3, nomination: "Pack 3", description: "Pack 3 Details", discount: 20}
]
