/**
 * When the page is loaded the we will load the function getPackages()
 */
window.addEventListener('load', () => {
    getPackages();
})


/**
 * Will request the server, and then, we can call it whanever we want.
 */
const getPackages = () => {
    fetch('http://localhost:8899/packages').then((res) => {
        res.json().then((packages) => {
            updateViewPackages(packages)
        })
    })
}


/**
 * Update the view, we receive all the packages stored in our database. DOM manipulation.
 * 
 * @param {Array} spaces 
 */
const updateViewPackages = (packages) => {
    let template = document.querySelector("#template-packages");
    
    packages.forEach((v, i) => {
        let clone = document.importNode(template.content, true);
        
        clone.querySelector("span").textContent = v.nomination;
        clone.querySelector("li").textContent = v.description;
        clone.querySelector("a").setAttribute("href", `http://localhost/package-detail.html?package=${v.id}`)
        clone.querySelector("#package-discount").textContent = v.discount + "%";
        
        document.querySelector("#packages-content").appendChild(clone);
    })
}